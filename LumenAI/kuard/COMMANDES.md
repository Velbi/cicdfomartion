# Quelques commandes

## ConfigMap

```shell
# charger un fichier de configuration :
# conf-prop correspond au nom du configmap
kubectl create configmap apache-conf --from-file=apache.conf

# charger les fichiers dans un répertoire :

# lister les configmaps :
kubectl get configmap
# supprime du configmap nommé apache-conf :
kubectl delete configmap apache-conf
```
