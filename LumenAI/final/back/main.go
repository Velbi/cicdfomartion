package main
 
import (
    "encoding/json"
    "fmt"
    "log"
	"net/http"
	"github.com/gorilla/mux"
	"strconv"
	"time"
)
 
type nb struct {
    nb     string `json:"nb"`
}
 
func homePage(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Welcome to the HomePage!")
    fmt.Println("Endpoint Hit: homePage")
}
 
func handleRequests() {
    myRouter := mux.NewRouter().StrictSlash(true)
    myRouter.HandleFunc("/", homePage)
    myRouter.HandleFunc("/multiply/{a}/{b}",returnMutiplyOfAandB)
    log.Fatal(http.ListenAndServe(":8000", myRouter))
}
 
func returnMutiplyOfAandB(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	vars := mux.Vars(r)
	nba, _ :=  strconv.Atoi(vars["a"])
	nbb, _ :=  strconv.Atoi(vars["b"])
	result:=nba*nbb
	json.NewEncoder(w).Encode(result)
	fmt.Println(time.Now())
	fmt.Println(result)
	fmt.Println("Endpoint Hit: multiply")

}


func main() {
    
    handleRequests()
}