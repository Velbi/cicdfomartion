minikube start --driver=docker
minikube docker-env
eval $(minikube -p minikube docker-env)
docker build -t antoine/appgo back/.
kubectl create -f podgo.yml
kubectl apply -f deploygo.yml

docker build -t antoine/appjs front/.
kubectl create -f podjs.yml
kubectl apply -f deployjs.yml

#kubectl port-forward appgo 8000:8000
#kubectl port-forward appjs 80:80
minikube service go-appgo-service --url


kubectl delete -f MyApp.yml
kubectl apply -f MyApp.yml

kubectl get pods
kubectl get services

kubectl delete service --all
kubectl delete pods --all
kubectl delete ingress --all


docker run -d -p 80:80 --name runappjs antoine/appjs
docker stop runappjs && docker rm runappjs
docker run -it --rm -d -p 80:80 --name runappjs antoine/appjs

minikube ssh
docker pull quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.33.0
minikube addons enable ingress
kubectl apply -f ingress.yml
kubectl get ingress