# Install wsl1 puis wsl2
- powerrshell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all
- Run wsl_updatex64.msi
wsl --set-default-version 2
# connect wsl2 to vagrant
sudo apt install vagrant